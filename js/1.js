let paragraphs = document.getElementsByTagName("p");
for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = "#ff0000";
}

let optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentNode);
let childNodes = optionsList.childNodes;
for (let i = 0; i < childNodes.length; i++) {
    console.log(childNodes[i].nodeName + " " + childNodes[i].nodeType);
}

let testParagraph = document.querySelector(".options-list-text#testParagraph");
testParagraph.innerHTML = "<p>This is a paragraph<p/>";

let mainHeader = document.querySelector(".main-header");
let listItems = mainHeader.getElementsByTagName("li");
for (let i = 0; i < listItems.length; i++) {
    listItems[i].classList.add("nav-item");
    console.log(listItems[i]);
}

let sectionTitles = document.querySelectorAll(".section-title");
for (let i = 0; i < sectionTitles.length; i++) {
    sectionTitles[i].removeAttribute("class");
}